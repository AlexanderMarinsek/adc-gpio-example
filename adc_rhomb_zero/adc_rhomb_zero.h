/**
 * @brief       Rhomb-Zero specific ADC peripheral driver interface
 *
 * ADC driver for Rhomb Zero board
 *
 * @todo        Finish non-blocking sample routine with common state, which
 * 				can be shared between multiple ADC lines (if needed).
 *
 * @{
 *
 * @file
 * @brief       Rhomb-Zero specific ADC peripheral driver interface definitions
 */

#ifndef ADC_RHOMB_ZERO_H
#define ADC_RHOMB_ZERO_H

#include "periph/adc.h"


/* ADC_STATES */
#define ADC_STATE_DISABLED					0
#define ADC_STATE_ENABLED					1
#define ADC_STATE_INPUT_SELECTED			2
#define ADC_STATE_CONVERSION_STARTED		3


/**
 * @brief   Configure ADC MUX
 *
 * Configure on-board multiplexer to expose ADC pins instead of 1-wire pins
 *
 * @return                  0 on success
 */
int adc_mux_init_custom(void);

/**
 * @brief   Configure the ADC
 *
 * Use a fixed resolution value for all conversions, isable interrupts,
 * set voltage reference , etc. In the end leave the ADC enabled. By doing so,
 * future sampling times are reduce by avoiding additional synchronisations.
 *
 * @param[in] res           resolution to use for all conversions
 *
 * @return                  0 on success
 */
int adc_config_custom(adc_res_t res);

/**
 * @brief   Init external line as ADC input
 *
 * Configure a GPIO pin as the ADC's input. The pin is accessed through the
 * line variable.
 *
 * @param[in] line          line to sample
 *
 * @return                  0 on success
 */
int adc_gpio_init_custom(adc_t line);

/**
 * @brief   Sample a value from the given ADC line
 *
 * Sample the line, which can carry an internal, or external analog value.
 * This procedure is blocking, but with few synchronisations.
 *
 * Execution times (equal for int. and external signals, including printf):
 * -without enable/disable < 70 us
 * -with enable/disable < 80 us
 *
 * @param[in] line          line to sample
 *
 * @return                  the sampled value on success
 */
int adc_sample_blocking_custom(adc_t line);

/**
 * @brief   Sample a value from the given ADC line
 *
 * Sample the line, which can carry an internal, or external analog value.
 * This procedure is non-blocking.
 *
 * @param[in] line          line to sample
 *
 * @return                  the sampled value on success
 */
int adc_sample_custom(adc_t line);

#endif /* ADC_RHOMB_ZERO_H */
/** @} */
