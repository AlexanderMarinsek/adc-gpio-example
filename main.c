
/* Include from boards > arduino-zero > include */
//#include "arduino_pinmap.h"
/* Include from drivers > include > periph */
#include "periph/gpio.h"
#include "periph/adc.h"
#include "xtimer.h"

#include <stdio.h>

#include "adc_rhomb_zero/adc_rhomb_zero.h"

int main (void) {
	volatile int a0 = 0;
	volatile int a1 = 0;
	volatile int a2 = 0;
	volatile int a3 = 0;
	volatile int a4 = 0;
	volatile int a5 = 0;
	volatile int a6 = 0;
	volatile int a7 = 0;

	// LED
	gpio_init (GPIO_PIN(PA, 7), GPIO_OUT);

	// Configure ADC
	adc_config_custom (ADC_RES_8BIT);

	adc_gpio_init_custom (ADC_LINE(0));
	adc_gpio_init_custom (ADC_LINE(1));
	adc_gpio_init_custom (ADC_LINE(2));
	adc_gpio_init_custom (ADC_LINE(3));

	uint32_t tmp1 = xtimer_now_usec();
	uint32_t tmp2 = xtimer_now_usec();

	while (1) {
		gpio_clear (GPIO_PIN(PA, 7));
		xtimer_usleep(1000000);

		tmp1 = xtimer_now_usec();
		a0 = adc_sample_blocking_custom (ADC_LINE(0));
		tmp2 = xtimer_now_usec();
		printf("a0 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a1 = adc_sample_blocking_custom (ADC_LINE(1));
		tmp2 = xtimer_now_usec();
		printf("a1 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a2 = adc_sample_blocking_custom (ADC_LINE(2));
		tmp2 = xtimer_now_usec();
		printf("a2 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a3 = adc_sample_blocking_custom (ADC_LINE(3));
		tmp2 = xtimer_now_usec();
		printf("a3 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a4 = adc_sample_blocking_custom (ADC_LINE(4));
		tmp2 = xtimer_now_usec();
		printf("a4 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a5 = adc_sample_blocking_custom (ADC_LINE(5));
		tmp2 = xtimer_now_usec();
		printf("a5 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a6 = adc_sample_blocking_custom (ADC_LINE(6));
		tmp2 = xtimer_now_usec();
		printf("a6 time: %d\n", tmp2-tmp1);
		tmp1 = xtimer_now_usec();
		a7 = adc_sample_blocking_custom (ADC_LINE(7));
		tmp2 = xtimer_now_usec();
		printf("a7 time: %d\n", tmp2-tmp1);

		printf("A0: %d, A1: %d, A2: %d, A3: %d, "
				"A4: %d, A5: %d, A6: %d, A7: %d\n",
				a0, a1, a2, a3, a4, a5, a6, a7);

		gpio_set (GPIO_PIN(PA, 7));
		xtimer_usleep(1000000);
	}

	return 0;
}
